import { randomColor } from "./math.js"

export function geometryF3d() {
    return new Float32Array([
        // left column front
        0, 0, 0,
        0, 150, 0,
        30, 0, 0,
        0, 150, 0,
        30, 150, 0,
        30, 0, 0,

        // top rung front
        30, 0, 0,
        30, 30, 0,
        100, 0, 0,
        30, 30, 0,
        100, 30, 0,
        100, 0, 0,

        // middle rung front
        30, 60, 0,
        30, 90, 0,
        67, 60, 0,
        30, 90, 0,
        67, 90, 0,
        67, 60, 0,

        // left column back
        0, 0, 30,
        30, 0, 30,
        0, 150, 30,
        0, 150, 30,
        30, 0, 30,
        30, 150, 30,

        // top rung back
        30, 0, 30,
        100, 0, 30,
        30, 30, 30,
        30, 30, 30,
        100, 0, 30,
        100, 30, 30,

        // middle rung back
        30, 60, 30,
        67, 60, 30,
        30, 90, 30,
        30, 90, 30,
        67, 60, 30,
        67, 90, 30,

        // top
        0, 0, 0,
        100, 0, 0,
        100, 0, 30,
        0, 0, 0,
        100, 0, 30,
        0, 0, 30,

        // top rung right
        100, 0, 0,
        100, 30, 0,
        100, 30, 30,
        100, 0, 0,
        100, 30, 30,
        100, 0, 30,

        // under top rung
        30, 30, 0,
        30, 30, 30,
        100, 30, 30,
        30, 30, 0,
        100, 30, 30,
        100, 30, 0,

        // between top rung and middle
        30, 30, 0,
        30, 60, 30,
        30, 30, 30,
        30, 30, 0,
        30, 60, 0,
        30, 60, 30,

        // top of middle rung
        30, 60, 0,
        67, 60, 30,
        30, 60, 30,
        30, 60, 0,
        67, 60, 0,
        67, 60, 30,

        // right of middle rung
        67, 60, 0,
        67, 90, 30,
        67, 60, 30,
        67, 60, 0,
        67, 90, 0,
        67, 90, 30,

        // bottom of middle rung.
        30, 90, 0,
        30, 90, 30,
        67, 90, 30,
        30, 90, 0,
        67, 90, 30,
        67, 90, 0,

        // right of bottom
        30, 90, 0,
        30, 150, 30,
        30, 90, 30,
        30, 90, 0,
        30, 150, 0,
        30, 150, 30,

        // bottom
        0, 150, 0,
        0, 150, 30,
        30, 150, 30,
        0, 150, 0,
        30, 150, 30,
        30, 150, 0,

        // left side
        0, 0, 0,
        0, 0, 30,
        0, 150, 30,
        0, 0, 0,
        0, 150, 30,
        0, 150, 0,
    ]);
}

export function colorF3d() {
    const f = randomColor();
    const back = randomColor();
    const t = randomColor();
    const bottom = randomColor();
    const r = randomColor();
    const l = randomColor();
    return new Uint8Array([
        f.r, f.g, f.b,
        f.r, f.g, f.b,
        f.r, f.g, f.b,
        f.r, f.g, f.b,
        f.r, f.g, f.b,
        f.r, f.g, f.b,

        f.r, f.g, f.b,
        f.r, f.g, f.b,
        f.r, f.g, f.b,
        f.r, f.g, f.b,
        f.r, f.g, f.b,
        f.r, f.g, f.b,

        f.r, f.g, f.b,
        f.r, f.g, f.b,
        f.r, f.g, f.b,
        f.r, f.g, f.b,
        f.r, f.g, f.b,
        f.r, f.g, f.b,

        back.r, back.g, back.b,
        back.r, back.g, back.b,
        back.r, back.g, back.b,
        back.r, back.g, back.b,
        back.r, back.g, back.b,
        back.r, back.g, back.b,

        back.r, back.g, back.b,
        back.r, back.g, back.b,
        back.r, back.g, back.b,
        back.r, back.g, back.b,
        back.r, back.g, back.b,
        back.r, back.g, back.b,

        back.r, back.g, back.b,
        back.r, back.g, back.b,
        back.r, back.g, back.b,
        back.r, back.g, back.b,
        back.r, back.g, back.b,
        back.r, back.g, back.b,

        t.r, t.g, t.b,
        t.r, t.g, t.b,
        t.r, t.g, t.b,
        t.r, t.g, t.b,
        t.r, t.g, t.b,
        t.r, t.g, t.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        bottom.r, bottom.g, bottom.b,
        bottom.r, bottom.g, bottom.b,
        bottom.r, bottom.g, bottom.b,
        bottom.r, bottom.g, bottom.b,
        bottom.r, bottom.g, bottom.b,
        bottom.r, bottom.g, bottom.b,

        r.r, r.g, r.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,

        t.r, t.g, t.b,
        t.r, t.g, t.b,
        t.r, t.g, t.b,
        t.r, t.g, t.b,
        t.r, t.g, t.b,
        t.r, t.g, t.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        bottom.r, bottom.g, bottom.b,
        bottom.r, bottom.g, bottom.b,
        bottom.r, bottom.g, bottom.b,
        bottom.r, bottom.g, bottom.b,
        bottom.r, bottom.g, bottom.b,
        bottom.r, bottom.g, bottom.b,

        r.r, r.g, r.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        r.r, r.g, r.b,
        bottom.r, bottom.g, bottom.b,
        bottom.r, bottom.g, bottom.b,
        bottom.r, bottom.g, bottom.b,
        bottom.r, bottom.g, bottom.b,
        bottom.r, bottom.g, bottom.b,
        bottom.r, bottom.g, bottom.b,
        l.r, l.g, l.b,
        l.r, l.g, l.b,
        l.r, l.g, l.b,
        l.r, l.g, l.b,
        l.r, l.g, l.b,
        l.r, l.g, l.b,
    ]);
}

export function geometryF() {
    return new Float32Array([
        // left column
        0, 0,
        30, 0,
        0, 150,
        0, 150,
        30, 0,
        30, 150,

        // top rung
        30, 0,
        100, 0,
        30, 30,
        30, 30,
        100, 0,
        100, 30,

        // middle rung
        30, 60,
        67, 60,
        30, 90,
        30, 90,
        67, 60,
        67, 90,
    ]);
}