import { createShader, createProgram, resizeCanvasToDisplaySize } from "./utils.js"
import { m3, m4, randomInt } from "./math.js"
import { createSlider } from "./ui.js"
import { colorF3d, geometryF, geometryF3d } from "./data.js"

async function load() {
    const [vsh, fsh] = await Promise.all([loadScript("demo3d.vs"), loadScript("demo3d.fs"), new Promise((resolve) => {
        window.addEventListener("load", resolve)
    })])
    return {
        vsh: vsh,
        fsh: fsh
    }
}

load().then((sources) => {
    console.log(sources);

    const canvas = document.getElementById("canvas");
    /** @type {WebGL2RenderingContext} */
    const gl = canvas.getContext("webgl2");
    if (!(gl && gl instanceof WebGL2RenderingContext)) {
        console.log("WebGL2 context is not available.");
        return
    }

    resizeCanvasToDisplaySize(gl);
    window.addEventListener("resize", () => {
        resizeCanvasToDisplaySize(gl);
        draw()
    })
    // Clear the canvas
    gl.clearColor(0, 0, 0, 0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.enable(gl.DEPTH_TEST);
    gl.enable(gl.CULL_FACE);

    const vertexShader = createShader(gl, gl.VERTEX_SHADER, sources.vsh);
    const fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, sources.fsh);

    const program = createProgram(gl, vertexShader, fragmentShader);
    gl.useProgram(program);

    const positionAttributeLocation = gl.getAttribLocation(program, "a_position");
    const colorAttributeLocation = gl.getAttribLocation(program, "a_color");
    console.log("position attribute: " + positionAttributeLocation);
    console.log("color attribute: " + colorAttributeLocation);

    const colorLocation = gl.getUniformLocation(program, "u_color");

    const positionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    // setRectangle(gl, 100, 100, 100, 200);
    
    const vao = gl.createVertexArray()
    gl.bindVertexArray(vao)
    setF3D(gl)
    
    gl.enableVertexAttribArray(positionAttributeLocation)

    // Setting how to pull data form array buffer
    const size = 3;
    const type = gl.FLOAT;
    const normalize = false;
    const stride = 0;
    const offset = 0;
    // NOTE: This also binds current ARRAY_BUFFER to the attribute. E.g. we can bind another
    // buffer after this call.
    gl.vertexAttribPointer(positionAttributeLocation, size, type, normalize, stride, offset)

    const colorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, colorF3d(), gl.STATIC_DRAW);

    gl.enableVertexAttribArray(colorAttributeLocation);
    gl.vertexAttribPointer(colorAttributeLocation, 3, gl.UNSIGNED_BYTE, true, 0, 0);

    setColor(gl, colorLocation);
    // drawRectangle(gl, colorLocation);

    const translation = [150, 150, 0];
    const rotation = [45, 0, 15]
    const scale = [1, 1.2, 1]

    draw();

    const controls = document.getElementById("controls");
    document.body.appendChild(controls);
    createSlider(controls, {
        id: "tx-f",
        name: "x",
        value: translation[0],
        max: gl.canvas.width,
        handler: function (value) {
            translation[0] = value;
            draw();
        }
    });
    createSlider(controls, {
        id: "ty-f",
        name: "y",
        value: translation[1],
        max: gl.canvas.height,
        handler: function (value) {
            translation[1] = value;
            draw();
        }
    });
    createSlider(controls, {
        id: "tz-f",
        name: "z",
        value: translation[2],
        max: 400,
        handler: function (value) {
            translation[2] = value;
            draw();
        }
    });
    createSlider(controls, {
        id: "rx-f",
        name: "angle-x",
        value: rotation[0],
        max: 360,
        handler: function (value) {
            rotation[0] = value;
            draw();
        }
    });
    createSlider(controls, {
        id: "ry-f",
        name: "angle-y",
        value: rotation[1],
        max: 360,
        handler: function (value) {
            rotation[1] = value;
            draw();
        }
    });
    createSlider(controls, {
        id: "rx-f",
        name: "angle-z",
        value: rotation[2],
        max: 360,
        handler: function (value) {
            rotation[2] = value;
            draw();
        }
    });
    createSlider(controls, {
        id: "sx-f",
        name: "scale-x",
        value: scale[0],
        min: -5,
        max: 5,
        step: 0.01,
        precision: 2,
        handler: function (value) {
            scale[0] = value;
            draw();
        }
    });
    createSlider(controls, {
        id: "sy-f",
        name: "scale-y",
        value: scale[1],
        min: -5,
        max: 5,
        step: 0.01,
        precision: 2,
        handler: function (value) {
            scale[1] = value;
            draw();
        }
    });
    createSlider(controls, {
        id: "sz-f",
        name: "scale-z",
        value: scale[2],
        min: -5,
        max: 5,
        step: 0.01,
        precision: 2,
        handler: function (value) {
            scale[2] = value;
            draw();
        }
    });

    function draw() {
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        const transformLocation = gl.getUniformLocation(program, "u_transform");

        const projection = m4.orthographic(0, gl.canvas.width, 0, gl.canvas.height, 400, -400);

        const translated = m4.translate(projection, translation);
        let rotated = m4.rotateX(translated, rotation[0]);
        rotated = m4.rotateY(rotated, rotation[1]);
        rotated = m4.rotateZ(rotated, rotation[2]);
        const scaled = m4.scale(rotated, scale);
        gl.uniformMatrix4fv(transformLocation, false, scaled);

        gl.drawArrays(gl.TRIANGLES, 0, 16 * 6);

        console.log(gl.getError());
    }
})

function loadScript(path) {
    const result = new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest()
        xhr.open("GET", path)
        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    resolve(xhr.responseText)
                } else {
                    console.error("Can't load script.", xhr.status);
                    reject(xhr.status)
                }
            }
        };
        xhr.send();
    })
    return result;
}

function drawRectangle(gl) {
    gl.drawArrays(gl.TRIANGLES, 0, 6);
}

function setColor(gl, colorLocation) {
    gl.uniform4f(colorLocation, Math.random(), Math.random(), Math.random(), 1);
}

// Fills the buffer with the values that define a rectangle.
function setRectangle(gl, x, y, width, height) {
    var x1 = x;
    var x2 = x + width;
    var y1 = y;
    var y2 = y + height;

    // NOTE: gl.bufferData(gl.ARRAY_BUFFER, ...) will affect
    // whatever buffer is bound to the `ARRAY_BUFFER` bind point
    // but so far we only have one buffer. If we had more than one
    // buffer we'd want to bind that buffer to `ARRAY_BUFFER` first.

    gl.bufferData(
        gl.ARRAY_BUFFER,
        new Float32Array([
            x1, y1,
            x2, y1,
            x1, y2,
            x1, y2,
            x2, y1,
            x2, y2]),
        gl.STATIC_DRAW
    );
}

function setF(gl) {
    gl.bufferData(gl.ARRAY_BUFFER, geometryF(), gl.STATIC_DRAW);
}

function setF3D(gl) {
    gl.bufferData(gl.ARRAY_BUFFER, geometryF3d(), gl.STATIC_DRAW);
}

function drawRandomRectangles(gl, colorLocation) {
    // draw 50 random rectangles in random colors
    for (var ii = 0; ii < 50; ++ii) {
        // Setup a random rectangle
        setRectangle(gl, randomInt(300), randomInt(300), randomInt(300), randomInt(300));

        // Set a random color.
        gl.uniform4f(colorLocation, Math.random(), Math.random(), Math.random(), 1);

        // Draw the rectangle.
        var primitiveType = gl.TRIANGLES;
        gl.drawArrays(primitiveType, 0, 6);
    }
}