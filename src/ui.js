/**
 * 
 * @param {HTMLDivElement} element 
 * @param {{ 
 *      id: string,
 *      min?: number,
 *      max?: number,
 *      name?: string,
 *      value?: number,
 *      step?: number,
 *      precision?: number,
 *      handler?: (value: number) => void
 * }} options 
 * @returns 
 */
export function createSlider(element, options) {
    const min = options.min || 0;
    const max = options.max || 1;
    const id = options.id;
    const name = options.name || id;
    const value = options.value || min;
    const step = options.step || 1;
    const precision = options.precision || 0;
    const handler = options.handler || (() => {});

    const input = document.createElement("input");
    input.setAttribute("id", id)
    input.setAttribute("class", "slider")
    input.setAttribute("type", "range");
    input.setAttribute("min", min);
    input.setAttribute("max", max);
    input.setAttribute("value", value);
    input.setAttribute("step", step);

    const valueDiv = document.createElement("div");
    valueDiv.className = "controls-value";
    updateText(value);

    function updateText(value) {
        valueDiv.textContent = value.toFixed(precision);
    }

    function updateValue(event) {
        const value = Number(event.target.value);
        updateText(value);
        handler(value);
    }

    input.addEventListener("change", updateValue);
    input.addEventListener("input", updateValue);

    const nameDiv = document.createElement("div");
    nameDiv.textContent = name;
    nameDiv.className = "controls-name";

    const rowContainer = document.createElement("div");
    rowContainer.className = "controls-row";
    rowContainer.appendChild(nameDiv);
    rowContainer.appendChild(input);
    rowContainer.appendChild(valueDiv);
    
    element.appendChild(rowContainer);

    return {
        element: element,
        updateValue: (v) => {
            const value = Math.round(v / step) * step
            updateText(value)
            input.value = value;
        }
    }
}