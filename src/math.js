export const m3 = {
    multiply: function (a, b) {
        return [
            a[0] * b[0] + a[1] * b[3] + a[2] * b[6],
            a[0] * b[1] + a[1] * b[4] + a[2] * b[7],
            a[0] * b[2] + a[1] * b[5] + a[2] * b[8],

            a[3] * b[0] + a[4] * b[3] + a[5] * b[6],
            a[3] * b[1] + a[4] * b[4] + a[5] * b[7],
            a[3] * b[2] + a[4] * b[5] + a[5] * b[8],

            a[6] * b[0] + a[7] * b[3] + a[8] * b[6],
            a[6] * b[1] + a[7] * b[4] + a[8] * b[7],
            a[6] * b[2] + a[7] * b[5] + a[8] * b[8],
        ]
    },
    // We use transposed matricies because in GLSL API martices are column-major,
    // but we store them as list so they get effectively trasposed when passing to GLSL.
    translation: function (dx, dy) {
        return [
            1, 0, 0,
            0, 1, 0,
            dx, dy, 1
        ]
    },
    scale: function (sx, sy) {
        return [
            sx, 0, 0,
            0, sy, 0,
            0, 0, 1
        ]
    },
    rotation: function (degrees) {
        const radians = degreesToRadians(degrees);
        const s = Math.sin(radians);
        const c = Math.cos(radians);
        return [
            c, s, 0,
            -s, c, 0,
            0, 0, 1
        ]
    },
    projection: function (w, h) {
        return [
            2 / w, 0, 0,
            0, -2 / h, 0,
            -1, 1, 1
        ]
    }
}

export const m4 = {
    projection: function (w, h, d) {
        return [
            2 / w, 0, 0, 0,
            0, -2 / h, 0, 0,
            0, 0, 2 / d, 0,
            -1, 1, 0, 1
        ]
    },
    orthographic: function (left, right, top, bottom, near, far) {
        const w = right - left;
        const h = bottom - top;
        const d = near - far;
        return [
            2 / w, 0, 0, 0,
            0, -2 / h, 0, 0,
            0, 0, 2 / d, 0,
            -(right + left) / w, (bottom + top) / h, (near + far) / d, 1
        ]
    },
    rotationValues: function (degrees) {
        const radians = degreesToRadians(degrees);
        const s = Math.sin(radians);
        const c = Math.cos(radians);
        return [s, c];
    },
    rotationZ: function (degrees) {
        const [s, c] = m4.rotationValues(degrees);
        return [
            c, s, 0, 0,
            -s, c, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
        ]
    },
    rotateZ: function (matrix, degrees) {
        return m4.multiply(m4.rotationZ(degrees), matrix);
    },
    rotationY: function (degrees) {
        const [s, c] = m4.rotationValues(degrees);
        return [
            c, 0, -s, 0,
            0, 1, 0, 0,
            s, 0, c, 0,
            0, 0, 0, 1
        ]
    },
    rotateY: function (matrix, degrees) {
        return m4.multiply(m4.rotationY(degrees), matrix);
    },
    rotationX: function (degrees) {
        const [s, c] = m4.rotationValues(degrees);
        return [
            1, 0, 0, 0,
            0, c, s, 0,
            0, -s, c, 0,
            0, 0, 0, 1
        ]
    },
    rotateX: function (matrix, degrees) {
        return m4.multiply(m4.rotationX(degrees), matrix);
    },
    scaling: function (sx, sy, sz) {
        return [
            sx, 0, 0, 0,
            0, sy, 0, 0,
            0, 0, sz, 0,
            0, 0, 0, 1
        ]
    },
    scale: function (matrix, [sx, sy, sz]) {
        return m4.multiply(m4.scaling(sx, sy, sz), matrix);
    },
    translation: function (dx, dy, dz) {
        return [
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            dx, dy, dz, 1
        ]
    },
    translate: function (matrix, [dx, dy, dz]) {
        return m4.multiply(m4.translation(dx, dy, dz), matrix);
    },
    multiply: function (a, b) {
        return [
            a[0] * b[0] + a[1] * b[4] + a[2] * b[8] + a[3] * b[12],
            a[0] * b[1] + a[1] * b[5] + a[2] * b[9] + a[3] * b[13],
            a[0] * b[2] + a[1] * b[6] + a[2] * b[10] + a[3] * b[14],
            a[0] * b[3] + a[1] * b[7] + a[2] * b[11] + a[3] * b[15],

            a[4] * b[0] + a[5] * b[4] + a[6] * b[8] + a[7] * b[12],
            a[4] * b[1] + a[5] * b[5] + a[6] * b[9] + a[7] * b[13],
            a[4] * b[2] + a[5] * b[6] + a[6] * b[10] + a[7] * b[14],
            a[4] * b[3] + a[5] * b[7] + a[6] * b[11] + a[7] * b[15],

            a[8] * b[0] + a[9] * b[4] + a[10] * b[8] + a[11] * b[12],
            a[8] * b[1] + a[9] * b[5] + a[10] * b[9] + a[11] * b[13],
            a[8] * b[2] + a[9] * b[6] + a[10] * b[10] + a[11] * b[14],
            a[8] * b[3] + a[9] * b[7] + a[10] * b[11] + a[11] * b[15],

            a[12] * b[0] + a[13] * b[4] + a[14] * b[8] + a[15] * b[12],
            a[12] * b[1] + a[13] * b[5] + a[14] * b[9] + a[15] * b[13],
            a[12] * b[2] + a[13] * b[6] + a[14] * b[10] + a[15] * b[14],
            a[12] * b[3] + a[13] * b[7] + a[14] * b[11] + a[15] * b[15]
        ]
    },
}

export function degreesToRadians(degrees) {
    return degrees * Math.PI / 180;
}

export function randomInt(range) {
    return Math.floor(Math.random() * range);
}

export function randomColor() {
    return {
        r: randomInt(256),
        g: randomInt(256),
        b: randomInt(256)
    }
}